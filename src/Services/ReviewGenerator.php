<?php

namespace App\Services;

use App\Entity\Review;

class ReviewGenerator {

	
	public function generate($review) {
		$stars = rand(1,5);
		$review->setStars($stars);
		$random_comments = array(
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
				"Fusce aliquet diam ac libero vehicula, sed aliquam risus faucibus.",
				"Pellentesque euismod velit ut erat pretium, nec lobortis nulla rutrum.",
				"Mauris sed sapien tristique, scelerisque nisi sed, imperdiet massa.",
				"Donec et quam vestibulum, aliquet sem sed, semper tortor."
			);

		$comment = array_rand($random_comments);
		$review->setComment($random_comments[$comment]);
	}
}