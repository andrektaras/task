<?php

namespace App\Services;

use App\Entity\Review;
use Doctrine\ORM\EntityManager;

class ReviewManager {
	
	/**
	 * @param EntityManager $em  The Doctrine EntityManager
	 */
	private $em;
	
	
	public function __construct(EntityManager $em) {
		$this->em = $em;
	}
	
	public function insert($review) {
		$this->em->persist ( $review );
		$this->em->flush ();
	}
}