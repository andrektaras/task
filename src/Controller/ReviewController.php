<?php

// src/Controller/ReviewController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Review;
use App\Services\ReviewManager;
use App\Services\ReviewGenerator;

class ReviewController extends Controller
{
    /**
     *
     * @Route("/hotel{Id}/today/review", name="generate_review")
     */
    public function generate($Id)
    {
    	$repositoryHotel = $this->getDoctrine()
		->getRepository('App:Hotel');

		$repositoryReview = $this->getDoctrine()
		->getRepository('App:Review');

		$hotel = $repositoryHotel->findById($Id);
		if (!$hotel) {
				return $this->render('bundles/twigbundles/exceptions/error404.html.twig');
		}

		$datetime = new \DateTime("now");
		$today = $datetime->format('Y-m-d');
		$result = $repositoryReview->findRandomByDay($Id,$today);

		if ($result){
			$reviewRandom = $result[0];

				return $this->render('bundles/twigbundles/reviews/random_review.html.twig', array(	
							'review' => $reviewRandom,
							'hotel' => $Id
					));
		}
		else{
				return $this->render('bundles/twigbundles/exceptions/reviewnotfound.html.twig');
		}
    }
}