<?php
// src/DataFixtures/AppFixtures.php
namespace App\DataFixtures;

use App\Entity\Hotel;
use App\Entity\Review;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use DateTime;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // creating hotels!

         $hotel1 = new Hotel();
         $hotel1->setName('Double Oak Hotel');
         $hotel1->setAdress('Magnolia Drive');
         $manager->persist($hotel1);

         $hotel2 = new Hotel();
         $hotel2->setName('Scarlet Thicket Hotel');
         $hotel2->setAdress('School Street');
         $manager->persist($hotel2);

         $hotel3 = new Hotel();
         $hotel3->setName('Riverside Time Hotel');
         $hotel3->setAdress('Park Avenue');
         $manager->persist($hotel3);

         $hotel4 = new Hotel();
         $hotel4->setName('Sanctuary Resort & Spa');
         $hotel4->setAdress('Primrose Lane');
         $manager->persist($hotel4);

         $hotel5 = new Hotel();
         $hotel5->setName('Lunar Cloak Hotel');
         $hotel5->setAdress('Main Street East');
         $manager->persist($hotel5);

         $review = new Review();
         $review->setHotel($hotel1);
         $review->setStars(rand(1,5));
         $review->setComment('Very nice stay here. Friendly people, lots to do in the hostel. Located next to a beautiful park. Loved the pinball machine.');
         $date = new \DateTime('2018-06-03');
         $review->setDate($date);
         $manager->persist($review);

         $review = new Review();
         $review->setHotel($hotel1);
         $review->setStars(rand(1,5));
         $review->setComment('Malisimo. El peor hostel que fui. Nada que ver como lo pintan en las fotos. El precio no lo vale');
         $date = new \DateTime();
         $review->setDate($date);
         $manager->persist($review);

         $review = new Review();
         $review->setHotel($hotel1);
         $review->setStars(rand(1,5));
         $review->setComment('It was fine for a one night stay. The rooms were cramped and the beds were noisy.');
         $date = new \DateTime();
         $review->setDate($date);
         $manager->persist($review);

         $review = new Review();
         $review->setHotel($hotel2);
         $review->setStars(rand(1,5));
         $review->setComment('It was fine for a one night stay. The rooms were cramped and the beds were noisy.');
         $date = new \DateTime();
         $review->setDate($date);
         $manager->persist($review);

         $review = new Review();
         $review->setHotel($hotel2);
         $review->setStars(rand(1,5));
         $review->setComment('My bed had no sheets though this was later addressed.');
         $date = new \DateTime('2018-06-03');
         $review->setDate($date);
         $manager->persist($review);

         $review = new Review();
         $review->setHotel($hotel2);
         $review->setStars(rand(1,5));
         $review->setComment('Toilets and showers always flooded with water down into the hallways.');
         $date = new \DateTime();
         $review->setDate($date);
         $manager->persist($review);

         $review = new Review();
         $review->setHotel($hotel3);
         $review->setStars(rand(1,5));
         $review->setComment('My bed had no sheets though this was later addressed.');
         $date = new \DateTime();
         $review->setDate($date);
         $manager->persist($review);

         $review = new Review();
         $review->setHotel($hotel3);
         $review->setStars(rand(1,5));
         $review->setComment('Toilets and showers always flooded with water down into the hallways.');
         $date = new \DateTime('2018-06-03');
         $review->setDate($date);
         $manager->persist($review);

         $review = new Review();
         $review->setHotel($hotel4);
         $review->setStars(rand(1,5));
         $review->setComment('It was fine for a one night stay. The rooms were cramped and the beds were noisy.');
         $date = new \DateTime();
         $review->setDate($date);
         $manager->persist($review);

         $review = new Review();
         $review->setHotel($hotel4);
         $review->setStars(rand(1,5));
         $review->setComment('Toilets and showers always flooded with water down into the hallways.');
         $date = new \DateTime();
         $review->setDate($date);
         $manager->persist($review);

         $review = new Review();
         $review->setHotel($hotel5);
         $review->setStars(rand(1,5));
         $review->setComment('Most of the reception staff are lovely but are all useless at helping with most things.');
         $date = new \DateTime();
         $review->setDate($date);
         $manager->persist($review);

         $review = new Review();
         $review->setHotel($hotel5);
         $review->setStars(rand(1,5));
         $review->setComment('The staff were friendly and helpful and the location is great.');
         $date = new \DateTime();
         $review->setDate($date);
         $manager->persist($review);

        $manager->flush();
    }
}